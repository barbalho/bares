//#include "../include/file.hpp"
#include "../include/stack.hpp"
#include "../include/queue.hpp"
#include "../include/deque.hpp"
#include <iostream>
#include <fstream>
using namespace  std;

bool readFile(string nameFile, string *content){
	ifstream arq;		      
    arq.open(nameFile);		
    if (arq.is_open() && arq.good()){
        arq >> *content;
        arq.close();
        return true;
    }
    return false;
}

bool clearFile(string nameFile){
	ofstream arq;		      
    arq.open(nameFile);		
    if (arq.is_open() && arq.good()){
        arq << "";
        arq.close();
        return true;
    }
    return false;
}

bool writeFile(string nameFile, string content){
	ofstream arq;		      
    arq.open(nameFile);		
    if (arq.is_open() && arq.good()){
        arq << content << endl;
        arq.close();
        return true;
    }
    return false;
}

template <typename TYPE>
bool writeNextLineFile(string nameFile, TYPE content){
	ofstream arq;		      
    arq.open(nameFile, fstream::app);		
    if (arq.is_open() && arq.good()){
        arq << content << endl;
        arq.close();
        return true;
    }
    return false;
}

bool readLinesFile(string nameFile, Queue<string> *fila){
	ifstream arq(nameFile);
	char ch; 
	string linha;
	while (arq.get(ch)){
		 switch (ch) 
         {
			case '\n':
			    if(linha.size()>0 )
				    fila->enqueue(linha);
				linha = "";
				break;
			 default:
				linha+=ch;
				break;
		}
	}
	return true;
}



