#include "../include/validation.hpp"


int toInt(string str){
	int value = 0;
	for(int i = 0; i < str.size(); i++){
		value += (pow(10, str.size()-i-1)) * (str[i] - 48);
	}
	return value;
}

bool validateOperando(string c){
	return ( c[0]>='0' && c[0]<='9');
}

bool validateOperando(char c){
	return ( c>='0' && c<='9');
}

bool validateOperador(char c){
	return ( c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == '%');
}

bool validateOperador(string c){
	return( c == "+" || c == "-" || c == "*" || c == "/"  || c == "^" || c == "%");
}

int validateParentese(string c){
	if( c == "(")
		return 1;
	else if( c == ")")
		return 2;
	else 
		return 0;
}

int validateParentese(char c){
	if( c == '(')
		return 1;
	else if( c == ')')
		return 2;
	else 
		return 0;
}

bool validateUnario(string symb){
	return symb=="u";
}

bool validateUnario(string symb, string tempSymb){
	return ((symb == "-" && (tempSymb == "" || validateOperador(tempSymb) || validateParentese(tempSymb)==1))|| symb=="u");
}

bool validateFilaTokens(Queue<string> *fila, string *error){
	Queue<string> fila2(*fila);
	Stack<int> pilhaParenteses;
	string msgLog = "";
	bool operando = false;
	bool operador = false;
	bool espaco = false;
	bool escopoAberto = false;
	string exp;
	string tempSymb = "";
	char c;
	int i = 0;
	while(!fila2.isEmpty()){
		exp = fila2.dequeue();
		c = exp[i];
		*error = "[ERROR] - COLUNA ";
		if(validateOperando(exp)){
			if(operando){
				*error += (i+48);
				*error += " - 0. Falta Operador";
				return false;
			}
			operador = false;
			operando = true;;
			if(toInt(exp)>32767){
				*error += (i+48);
				*error += " - 1. Constante Numérica Inválida";
				return false;
			}
		}else if(validateOperador(exp)){
			if(operador && !validateUnario(exp,tempSymb)){
				*error += (i+48);
				*error += " - 2. Falta Operando";
				return false;
			}
			operando = false;
			operador = true;
		}else if(validateParentese(exp)==1){
			if(operando){
				*error += (i+48);
				*error += " - 3. Falta Operador";
				return false;
			}
			pilhaParenteses.push(i);
			operando = false;
			escopoAberto = true;	
		}else if(validateParentese(exp)==2){
			if (pilhaParenteses.isEmpty()){
				*error += (i+48);
				*error += " - 4. Fechamento de escopo inválido";
				return false;
			}else if(operador){
				*error += (i+48+1);
				*error += " - 5. Falta Operando";
				return false;
			}
			pilhaParenteses.pop();
			operando = false;
			escopoAberto = false;
		}else{
			if(operador){
				*error += (i+48);
				*error += " - 6. Operador Inválido";
				return false;
			}else{
				*error += (i+48);
				*error += " - 7. Operando Inválido";
				return false;
			}
		}
		i++;
		tempSymb = exp;
	}
	if(validateOperador(exp)){
		*error += (i+48);
		*error += " - 8. Falta Operando";
		return false;
	 }
	if (!pilhaParenteses.isEmpty()){
		*error += (to_string(pilhaParenteses.pop()));
		*error += " - 9. Escopo Aberto";
		return false;
	}
	return true;
}