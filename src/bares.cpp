//#include "../include/file.hpp"
//#include "../include/bares.hpp"
#include "file.cpp"
#include "../include/stack.hpp"
#include "../include/queue.hpp"
#include "../include/deque.hpp"
#include "../include/validation.hpp"
#include <iostream>
using namespace  std;

/**
 * Realiza a opreção matemática desejada.
 * @param op Operador.
 * @param opn1 1º operando.
 * @param opn2 2º operando.
 * @return Resultado da operação.
*/
int arithmeticOperation(string op, int opn1, int opn2){
	switch(op[0]){
		case '+':
			return opn1 + opn2;
			break;
		case '-':
			return opn1 - opn2;
			break;
		case '/':
			return opn1 / opn2;
			break;
		case '*':
			return opn1 * opn2;
			break;
		case '%':
			return opn1 % opn2;
			break;
		case '^':
			return pow (opn1, opn2);
			break;
		default:
			break;
	}
}

/**
 * Verifica prioridade de operador entre dois operadores.
 * @param o1 1º operador.
 * @param o2 2º operador.
 * @return Valor booleano informando se o operador 1 tem prioridade sobre o operador 2.
*/
bool ordemOperador(string o1,string o2){
	if(o1 == "^")
		return true;
	else if((o1 == "*" || o1 == "/" || o1 == "%") && (o2 == "-" || o2 == "+"))
		return true;
	else if((o1 == "-" || o1 == "+") && (o2 == "-" || o2 == "+"))
		return true;
	else if((o1 == "*" || o1 == "/" || o1 == "%") && (o2 == "*" || o2 == "/" || o2 == "%"))
		return true;
	return false;
	
}

/**
 * Cria uma fila de tokens de uma expressão.
 * @param *fila Referência da fila que terá a expressão quebrada em tokens.
 * @param exp String com a expressão original.
 * @return Valor booleano informando se houve sucesso.
*/
bool tokenization(Queue<string> *fila, string exp){
	string temp = "";
	for(int i = 0; i < exp.size(); i++){
		if (validateOperando(exp[i])){
			temp += exp[i];
		}else if(exp[i] != ' '){
			if (temp != ""){
				fila->enqueue(temp);
				temp = "";
			}
			temp = exp[i];
			fila->enqueue(temp);
			temp = "";
		}
	}
	
	if (temp != "")
		fila->enqueue(temp);
		
	cout <<"Fila de Entrada" << fila << endl;
	return true;
}

/**
 * Mapeia o unário em no caractere u
 * @param *fila Expressão em tokens.
 * @return Valor booleano informando se houve sucesso.
*/
bool mapUnario(Queue<string> *fila){
	string tempSymb = "";
	Queue<string> mapFila;
	while(!fila->isEmpty()){
		string symb = fila->dequeue();
		if (validateUnario( symb, tempSymb))
			mapFila.enqueue("u");
		else
			mapFila.enqueue(symb);
		tempSymb = symb;
	}
	
	while(!mapFila.isEmpty()){
		fila->enqueue(mapFila.dequeue());
	}
	
	return true;
}

/**
 * Converte uma expressão in-fixa em uma pós-fixa.
 * @param *fila Expressão in-fixa.
 * @param *filaSaida Fila da expressão pós-fixa.
 * @return Fila com a expressão pós-fixa.
*/
Queue<string> *Infx2Posfx(Queue<string> *fila, Queue<string> *filaSaida){

	Stack<string> pilhaOper;

	Queue<string> *filaP = new Queue<string>();

	mapUnario(fila);

	Stack<string> pilhaUnario;
	
	while(!fila->isEmpty()){
		string symb = fila->dequeue();
		if(validateParentese(symb) == 1){
			Infx2Posfx(fila, filaSaida);
		}else if(validateParentese(symb) == 2){
			continue;
		}else if(validateOperando(symb)){
			filaSaida->enqueue(symb);
			if(!pilhaUnario.isEmpty() && fila->getSize()==0){
				while(!pilhaUnario.isEmpty()){
					filaSaida->enqueue(pilhaUnario.pop());
				}
			}
			
		}else if(validateUnario(symb)){
			pilhaUnario.push(symb);
		}else{
			while(!pilhaUnario.isEmpty()){
				filaSaida->enqueue(pilhaUnario.pop());
			}
			while(!pilhaOper.isEmpty() && (ordemOperador(pilhaOper.top(), symb))){
				if(ordemOperador(pilhaOper.top(), symb)){
					filaSaida->enqueue(pilhaOper.pop());
				}
			}
			pilhaOper.push(symb);
		}
	}

	while(!pilhaOper.isEmpty()){
		filaSaida->enqueue(pilhaOper.pop());
	}
	
	while(!pilhaUnario.isEmpty()){
		filaSaida->enqueue(pilhaUnario.pop());
	}
	
	return filaSaida;
}

/**
 * Avalia se a expressão informada é pós-fixa e calcula seu resultado.
 * @param *fila Fila com expressão a ser avaliada.
 * @return Resultado da expressão
*/
int AvalPosfixa(Queue<string> *fila){
	string symb;
	Stack<int> opn;
	int opn1, opn2;
	int resultado;

	while(!fila->isEmpty()){
		symb = fila->dequeue();
		if(validateOperando(symb)){
			int val = toInt(symb);
			opn.push(val);	
		}else if (validateUnario(symb)){
			opn.push(opn.pop()*-1);
			
		}else {
			opn2 = opn.pop();
			opn1 = opn.pop();
			resultado = arithmeticOperation(symb, opn1, opn2);
			opn.push(resultado);
		}
	}
	
	if (!opn.isEmpty())
		resultado = opn.pop();
		
	return resultado;
}

int main(int argc, char *args[]){
	
	Queue<string> *filaExpressoes = new Queue<string>();
	string in;
	string out;
	
	if(argc > 2){
		out = args[2];
		clearFile(out);
	}
	
	if(argc > 1){
		in = args[1];
		readLinesFile(in, filaExpressoes);
		while(!filaExpressoes->isEmpty()){
			string exp = filaExpressoes->dequeue();
			Queue<string> *fila = new Queue<string>();
			tokenization(fila, exp);
			string error;
			if (!validateFilaTokens(fila, &error)){
				writeNextLineFile(out, error);
				continue;
			}
			
			Queue<string> *filaSaida = new Queue<string>();
			Infx2Posfx(fila, filaSaida);
			cout << "Fila de Saida"<<filaSaida << endl;
			if(argc > 2){
				writeNextLineFile(out, AvalPosfixa(filaSaida));
			}else{
				cout << AvalPosfixa(filaSaida)<< endl;
			}
		}
	}
	
	return 0;
}