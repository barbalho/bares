#include "../include/bares.hpp"
#include "../include/queue.hpp"
#include <iostream>
using namespace  std;

int main(int argc, char *args[]){
	
	Queue<string> *filaExpressoes = new Queue<string>();
	string in;
	string out;
	
	if(argc > 2){
		out = args[2];
		clearFile(out);
	}
	
	if(argc > 1){
		in = args[1];
		readLinesFile(in, filaExpressoes);
		while(!filaExpressoes->isEmpty()){
		
			string exp = filaExpressoes->dequeue();
			
			//validateChar(exp);
			
			/*Fila de entrada*/
			Queue<string> *fila = new Queue<string>();	
			
			/*declara fila com expressão*/
			tokenization(fila, exp);
		
			string error;
			if (!validateFilaTokens(fila, &error)){
				writeNextLineFile(out, error);
				continue;
			}
			/*Armazena a fila PosFix de Saída*/
			Queue<string> *filaSaida = new Queue<string>();
		
			/*conversão de expressão inFix em posFix na fila de Saída*/
			Infx2Posfx(fila, filaSaida);
			cout << "Fila de Saida"<<filaSaida << endl;
			if(argc > 2){
				//writeNextLineFile(out, filaSaida->toString());
				writeNextLineFile(out, AvalPosfixa(filaSaida));
			}else{
				//cout << filaSaida->toString() << endl;
				cout << AvalPosfixa(filaSaida)<< endl;
			}
			//AvalPosfixa(filaSaida)
		}
	}
	return 0;
}