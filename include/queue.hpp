#ifndef __QUEUE__
#define __QUEUE__

#include "sequence.hpp"

template <typename TYPE>
class Queue {
	Sequence<TYPE> seq;

public:
	/**
	 * Construtor de fila.
	*/
	Queue()  {}
	
	/**
	 * Destrutor de fila.
	*/
	~Queue() {}

	/**
	 * Consulta o primeiro elemento da fila.
	 * @return Elemento que se encontra na cabeça da fila.
	*/
	TYPE front() {return seq.getFirst();}
	
	/**
	 * Adiciona um elemento ao final da fila.
	 * @param value TYPE com o valor a ser inserido.
	 * @return Valor booleano de sucesso na operação ou fracasso.
	*/
	bool enqueue(TYPE value) {return seq.addLast(value);}
	
	/**
	 * Sobrecarga de método para o caso de se desejar inserir uma fila ao final de uma.
	 * @param *q Referência da fila a ser adicionada.
	 * @return Valor booleano de sucesso na operação ou fracasso.
	*/
	bool enqueue(Queue<TYPE> *q) {
		for(int i = 0; i < q->getSize(); i++){
			seq.addLast(q->dequeue());
		}
		return true;
	}
	
	/**
	 * Remove o elemento da cabeça da fila.
	 * @return Valor removido.
	*/
	TYPE dequeue() {return seq.removeFirst();}

	/**
	 * Consulta o tamanho da fila.
	 * @return Valor inteiro indicando o tamanho da fila.
	*/
	int getSize() {return seq.getSize();}
	
	/**
	 * Consulta se a fila está vazia.
	 * @return Valor booleano indicando se a fila está vazia ou não.
	*/
	bool isEmpty() {return seq.isEmpty();}
	
	/**
	 * Imprime, na saída padrão, a fila.
	 * @return Void.
	*/
	void print() {seq.print();}
	
	/**
	 * Converte a fila para String.
	 * @return String da fila.
	*/
	string toString(){
		string s = "{ ";
		for (int i = 0; i < getSize(); i++){
			s+= seq.get(i);
			if(i+1!=getSize()){
				s+= ", ";
			}
		}
		s += " }";
		return s;
	}
	
	template<class U> 
	friend ostream& operator << (ostream &out, Queue<U>  *q){ 
		out << q->toString();
		return out; 
    };
	
	template<class U> 
	friend ostream& operator << (ostream &out, Queue<U> const &q){ 
		out << q.toString();
		return out; 
    };
};

#endif
