#ifndef __FILE__
#define __FILE__
#include <iostream>
#include <fstream>
#include "stack.hpp"
#include "queue.hpp"
#include "deque.hpp"
#include <string>
using namespace  std;

/**
 * Lêr todos os dados de um arquivo.
 * @param nameFile String com nome do arquivo a ser lido.
 * @param content referência do variável onde será salvo o conteudo do arquivo. 
 * @return Valor booleano de sucesso na operação ou fracasso.
 */
bool readFile(string nameFile, string *content);

/**
 * Limpa todo o conteúdo do arquivo
 * @param nameFile String com nome do arquivo
 * @return Valor booleano de sucesso na operação ou fracasso.
 */
bool clearFile(string nameFile);

/**
 * Escreve um conteudo qualquer no arquivo, substituindo o anterior.
 * @param nameFile String com nome do arquivo.
 * @param content expressão que será salvo no arquivo.
 * @return Valor booleano de sucesso na operação ou fracasso.
 */
bool writeFile(string nameFile, string content);

/**
 * Escreve um conteudo qualquer no arquivo, após o conteúdo anterior.
 * @param nameFile String com nome do arquivo.
 * @param content expressão que será salvo no arquivo de um tipo genérico.
 * @return Valor booleano de sucesso na operação ou fracasso.
 */
template <typename TYPE>
bool writeNextLineFile(string nameFile, TYPE content);

/**
 * Ler cada linha de um arquivo e armazena em uma lista passada por referência.
 * @param nameFile String com nome do arquivo.
 * @param fila expressões de cada linha do arquivo diferente de vazio.
 * @return Valor booleano de sucesso na operação ou fracasso.
 */	
bool readLinesFile(string nameFile, Queue<string> *fila);


#endif