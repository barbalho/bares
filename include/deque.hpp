#ifndef __DEQUE__
#define __DEQUE__

#include "sequence.hpp"

template <typename TYPE>
class Deque {
	Sequence<TYPE> seq;
public:
	/**
	 * Construtor de deque.
	*/
	Deque() {}
	
	/**
	 * Destrutor de deque.
	*/
	~Deque() {}

	/**
	 * Consulta qual o elemento da frente do deque.
	 * @return Elemento da cabeça do deque.
	*/
	TYPE front() {return seq.getFirst();}
	
	/**
	 * Consulta qual o elemento do fim do deque.
	 * @return Elemento da calda do deque.
	*/
	TYPE back() {return seq.getLast();}

	/**
	 * Adiciona um elemento na frente do deque.
	 * @param value TYPE com o valor a ser inserido.
	 * @return Valor booleano de sucesso na operação ou fracasso.
	*/
	bool pushFront(TYPE value) {return seq.addFirst(value);}

	/**
	 * Adiciona um elemento ao final do deque.
	 * @param value TYPE com o valor a ser inserido.
	 * @return Valor booleano de sucesso na operação ou fracasso.
	*/
	bool pushBack(TYPE value) {return seq.addLast(value);}

	/**
	 * Remove um elemento da frente do deque.
	 * @return Elemento removido.
	*/
	TYPE popFront() {return seq.removeFirst();}
	
	/**
	 * Remove um elemento do final do deque.
	 * @return Elemento removido.
	*/
	TYPE popBack() {return seq.removeLast();}

	/**
	 * Consulta o tamanho do deque.
	 * @return Valor inteiro especificando o tamanho do deque.
	*/
	int getSize() {return seq.getSize();}
	
	/**
	 * Consulta se o deque está vazio.
	 * @return Valor booleano informando se o deque está ou não vazio.
	*/
	bool isEmpty() {return seq.isEmpty();}
	
	/**
	 * Imprime, na saída padrão, o deque.
	 * @return Void.
	*/
	void print() {seq.print();}
	
	/**
	 * Converte o deque para String.
	 * @return String do deque.
	*/
	string toString(){
		string s = "{ ";
		for (int i = 0; i < getSize(); i++){
			s+= seq.get(i);
			if(i+1!=getSize()){
				s+= ", ";
			}
		}
		s += " }";
		return s;
	}
	
	template<class U> 
	friend ostream& operator << (ostream &out, Deque<U>  *q){ 
		out << q->toString();
		return out; 
    };
	
	template<class U> 
	friend ostream& operator << (ostream &out, Deque<U> const &q){ 
		out << q.toString();
		return out; 
    };
};

#endif
