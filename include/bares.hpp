
#include "stack.hpp"
#include "queue.hpp"
#include "deque.hpp"
#include "validation.hpp"
#include <iostream>
using namespace  std;

int arithmeticOperation(string op, int opn1, int opn2);

bool ordemOperador(string o1,string o2);

bool tokenization(Queue<string> *fila, string exp);

bool mapUnario(Queue<string> *fila);

Queue<string> *Infx2Posfx(Queue<string> *fila, Queue<string> *filaSaida);

int AvalPosfixa(Queue<string> *fila);




