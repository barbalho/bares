#ifndef __VALIDATION__
#define __VALIDATION__

#include <iostream>
#include <cmath>
#include "stack.hpp"
#include "queue.hpp"

using namespace  std;

/**
 * Converte uma string para inteiro.
 * @param str expressão a ser convertida.
 * @return Resultado da conversão.
 */	
int toInt(string str);

/**
 * Verifica se a expressão é um operando passando uma string.
 * @param str expressão a ser verificada.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */	
bool validateOperando(string c);

/**
 * Verifica se a expressão é um operando passando um caractere.
 * @param str caractere a ser verificada.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */	
bool validateOperando(char c);

/**
 * Verifica se a expressão é um operador passando um caractere.
 * @param str caractere a ser verificada.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */
bool validateOperador(char c);

/**
 * Verifica se a expressão é um operando passando uma string.
 * @param str expressão a ser verificada.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */
bool validateOperador(string c);

/**
 * Verifica se a expressão é um parentese passando uma string, e qual tipo "(" ou ")".
 * @param str expressão a ser verificada.
 * @return valor do tipo: 0 (não é parentese), 1 (parentese aberto), 2 (parentese fechado).
 */
int validateParentese(string c);

/**
 * Verifica se a expressão é um parentese passando uma char, e qual tipo "(" ou ")".
 * @param str expressão a ser verificada.
 * @return valor do tipo: 0 (não é parentese), 1 (parentese aberto), 2 (parentese fechado).
 */
int validateParentese(char c);

/**
 * Verifica se a expressão é um valor unário. já existe um pré-tratamento.
 * @param str expressão a ser verificada.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */
bool validateUnario(string symb);

/**
 * Verifica se a expressão é um valor unário. primeiro tratamento de unário de uma lista de tokens.
 * @param str expressão a ser verificada do elemento atual da lista.
 * @param str expressão a ser verificada do elemento anterior.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */
bool validateUnario(string symb, string tempSymb);

/**
 * Verifica se toda a expressão é válida. atribuindo erro, caso contrário.
 * @param fila fila de tokens a ser verificada.
 * @param error referência da string que será atribuido caso exista erro.
 * @return valor boleano da verificação: verdadeiro ou falso.
 */
bool validateFilaTokens(Queue<string> *fila, string *error);

#endif