#ifndef __STACK__
#define __STACK__

#include "sequence.hpp"

template <typename TYPE>
class Stack {
	Sequence<TYPE> seq;
	
public:
	/**
	 * Construtor de pilha.
	*/
	Stack()  {};
	
	/**
	 * Destrutor de pilha.
	*/
	~Stack() {};

	/**
	 * Consulta qual elemento está no topo da pilha.
	 * @return Elemento do topo da pilha.
	*/
	TYPE top() {return seq.getFirst();}
	
	/**
	 * Empilha o elemento no topo da pilha.
	 * @param value TYPE com o elemento que se deseja empilhar.
	 * @return Valor booleano de sucesso na operação ou fracasso.
	*/
	bool push(TYPE value) {return seq.addLast(value);}
	
	/**
	 * Desempilha o elemento do topo da pilha.
	 * @return Elemento removido.
	*/
	TYPE pop() {return seq.removeLast();}

	/**
	 * Consulta o tamanho da pilha.
	 * @return Valor inteiro informando o tamanho da pilha.
	*/
	int getSize() {return seq.getSize();}
	
	/**
	 * Consulta se a pilha está vazia.
	 * @return Valor booleano informando se a pilha está ou não vazia.
	*/
	bool isEmpty() {return seq.isEmpty();}
	
	/**
	 * Imprime a pilha.
	 * @return Void.
	*/
	void print() {seq.print();}
	
	/**
	 * Converte a pilha para String.
	 * @return A pilha em String.
	*/
	string toString(){
		string s = "{ ";
		for (int i = 0; i < getSize(); i++){
			s+= seq.get(i);
			if(i+1!=getSize()){
				s+= ", ";
			}
		}
		s += " }";
		return s;
	}
	
	//template<class U> 
	//friend ostream& operator << (ostream &out, Stack<U>  *q){ 
	//	out << q->toString();
		//return out; 
   // };
	
//	template<class U> 
//	friend ostream& operator << (ostream &out, Stack<U> const &q){ 
	//	out << q.toString();
	//	return out; 
   // };
};

#endif
