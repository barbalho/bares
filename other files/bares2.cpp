#include <iostream>
#include <cmath>
#include "tads/stack.hpp"
#include "tads/queue.hpp"
#include "tads/deque.hpp"
#include "file.cpp"
#include "validation.cpp"
using namespace  std;

//g++ bares.cpp -std=c++0x -o app
//./app "4+5"

int operacao(string op, int opn1, int opn2){
	switch(op[0]){
		case '+':
			return opn1 + opn2;
			break;
		case '-':
			return opn1 - opn2;
			break;
		case '/':
			return opn1 / opn2;
			break;
		case '*':
			return opn1 * opn2;
			break;
		case '%':
			return opn1 % opn2;
			break;
		case '^':
			return pow (opn1, opn2);
			break;
		default:
			break;
	}
	
	
}

/*Verifica se Operador 1 ≥ Operador 2*/
bool ordemOperador(string o1,string o2){
	//logger("ORDEM-OPERADOR","TESTE ENTRE DOIS OPERADORES REALIZADO");
	if(o1 == "^")
		return true;
	else if((o1 == "*" || o1 == "/" || o1 == "%") && (o2 == "-" || o2 == "+"))
		return true;
	else if((o1 == "-" || o1 == "+") && (o2 == "-" || o2 == "+"))
		return true;
	else if((o1 == "*" || o1 == "/" || o1 == "%") && (o2 == "*" || o2 == "/" || o2 == "%"))
		return true;
	return false;
	
}



/*Adiciona a expressão de entrada em uma Fila*/
bool tokenString(Queue<string> *fila, string exp){
	string temp = "";
	for(int i = 0; i < exp.size(); i++){

		if (validateOperando(exp[i])){
			temp += exp[i];
		//}else if (validateOperador(exp[i]) || (validateParentese(exp[i]) != 0)){
		}else if(exp[i] != ' '){
			if (temp != ""){
				fila->enqueue(temp);
				temp = "";
			}
			temp = exp[i];
			fila->enqueue(temp);
			temp = "";
		}
	}
	if (temp != "")
		fila->enqueue(temp);
	cout << fila << endl;
	return true;
}

Queue<string> *Infx2Posfx(Queue<string> *fila, Queue<string> *filaSaida){
	/*Armazena os Operadores*/
	Stack<string> pilhaOper;
	/*Armazena a expressão dentro dos parenteses*/
	Queue<string> *filaP = new Queue<string>();
	/*enquanto não chegar ao fim da fila de entrada faça:*/
	while(!fila->isEmpty() && validateParentese(fila->front()) != 2){
		/*remover símbolo da fila de entrada em symb*/
		string symb = fila->dequeue();
		/*se symb for operando então*/
		if(!validateOperador(symb)){
			/*enviar symb diretamente para fila de saída*/
			filaSaida->enqueue(symb);
		/*senão*/
		}else{
			while(!pilhaOper.isEmpty() && ordemOperador(pilhaOper.top(), symb)){
				/*se topSym ≥ symb então*/
				if(ordemOperador(pilhaOper.top(), symb))
					/*remover topSym e enviar para fila de saída*/
					filaSaida->enqueue(pilhaOper.pop());
			}
			/*Empilhar symb*/
			pilhaOper.push(symb);
		}
	}
	/*enquanto Pilha não estiver vazia faça*/
	while(!pilhaOper.isEmpty()){
		/*remover símbolo da pilha e enviar para fila de saída*/
		filaSaida->enqueue(pilhaOper.pop());
	}
	/*retorna fila de saída na forma posfixa*/
	return filaSaida;
}


Queue<string> *AnalisaEncadeamento(Queue<string> *fila, Queue<string> *filaSaida){
    
		cout << fila << endl;
		string symb = fila->front();
		if(validateParentese(symb) == 2){
		    symb = fila->dequeue();
		    cout << "QC" << endl;
		    return filaSaida;
		}else if(validateParentese(symb) == 1){
		    symb = fila->dequeue();
		    AnalisaEncadeamento(fila, filaSaida);
		}
		    Infx2Posfx(fila, filaSaida);
		    cout << "Frente da fila: "<< fila->front() << endl;
		    if(fila->getSize() > 1)
		    fila->dequeue();
		    cout << "FILA DE SAIDA: "<< filaSaida << endl;
		
		    return filaSaida;	
    
    
}





int AvalPosfixa(Queue<string> *fila){
	//cout << fila << endl;
	string symb;
	Stack<int> opn;
	int opn1, opn2;
	int resultado;
	while(!fila->isEmpty()){
		symb = fila->dequeue();

		if(!validateOperador(symb)){
			int val = toInt(symb);
			opn.push(val);	
		}else{
			opn2 = opn.pop();
			opn1 = opn.pop();
			resultado = operacao(symb, opn1, opn2);
			opn.push(resultado);
		}
	}
	resultado = opn.pop();
	
	return resultado;
}





int main(int argc, char *args[]){
	
	Queue<string> *filaExpressoes = new Queue<string>();
	string in;
	string out;
	
	if(argc > 2){
		out = args[2];
		clearFile(out);
	}
	
	if(argc > 1){
		in = args[1];
		readLinesFile(in, filaExpressoes);
		while(!filaExpressoes->isEmpty()){
		
			string exp = filaExpressoes->dequeue();
			
			
			//validateChar(exp);
			
			/*Fila de entrada*/
			Queue<string> *fila = new Queue<string>();	
			
			/*declara fila com expressão*/
			tokenString(fila, exp);
		
		
			string error;
			if (!validateChar(fila, &error)){
				writeNextLineFile(out, error);
				continue;
			}
			
			
			/*Armazena a fila PosFix de Saída*/
			Queue<string> *filaSaida = new Queue<string>();
		
			/*conversão de expressão inFix em posFix na fila de Saída*/
			AnalisaEncadeamento(fila, filaSaida);
			
			if(argc > 2){
				//writeNextLineFile(out, filaSaida->toString());
				writeNextLineFile(out, AvalPosfixa(filaSaida));
			}else{
				//cout << filaSaida->toString() << endl;
				cout << AvalPosfixa(filaSaida)<< endl;
			}
				
			//AvalPosfixa(filaSaida)

		}
	}

	return 0;
}