
#include <iostream>

#include "stack.hpp"
#include "queue.hpp"
#include "deque.hpp"
//g++ main.cpp sequence.cpp -std=c++0x -o app
//g++ main.cpp -std=c++0x -o app
using namespace  std;

void exibir(string s){
	cout << s << endl;
}

int main() {
	Stack<int> s;
	Queue<int> q;
	Deque<int> d;

	int array[] = { 5, 2, 8, 1, 4, 6, 9, 3, 7, 3 };

	for (int i = 0; i < 10; ++i) {
		s.push(array[i]);
		q.enqueue(array[i]);
		d.pushFront(array[i]);
	}
		
	//Testes
	
	Sequence<int> seq;
	exibir("+----------------------------------+");
	exibir("-----isEmpty------");
	cout << seq.isEmpty() << endl;

	exibir("-----GetSize------");
	cout << seq.getSize() << endl;
	
	exibir("-----addFirst------");
	for (int i = 0; i < 7; ++i) {
		seq.addFirst(array[i]);
	}
	seq.print();
	
	exibir("-----addLast------");
	seq.addLast(7);
	seq.addLast(9);
	seq.addLast(5);
	seq.print();
	
	exibir("-----add(10,0)------");
	seq.add(1,0);
	seq.print();
	
	exibir("-----removeFirst------");
	seq.removeFirst();
	seq.print();
	
	exibir("-----removeLast------");
	seq.removeLast();
	seq.print();
	
	exibir("-----remove(1)------");
	seq.remove(1);
	seq.print();
	
	exibir("-----getFirst------");
	cout << seq.getFirst() << endl;
	
	
	exibir("-----getLast------");
	cout << seq.getLast() << endl;
	
	exibir("-----get(2)------");
	cout << seq.get(2) << endl;
	
	exibir("-----search(1)------");
	cout << seq.search(1) << endl;
	
	exibir("-----isEqual(seq)------");
	cout << seq.isEqual(seq) << endl;
	
	exibir("-----reverse()------");
	seq.reverse();
	seq.print();
	
	exibir("-----sort()------");
	seq.sort();
	seq.print();

	exibir("-----isIncreasing()------");
	cout << seq.isIncreasing() << endl;
	
	exibir("-----isDecreasing()------");
	cout << seq.isDecreasing() << endl;
	
	exibir("-----isDecreasing()------");
	cout << seq.isDecreasing() << endl;
	
	exibir("-----bounds()------");
	int min,max;
	seq.bounds(min, max);
	cout << min << " ~ " << max << endl;

	exibir("-----a==b------");
	Sequence<int> a;
	Sequence<int> b;
	if(a==b)
		cout << "São Iguais!" << endl;
	exibir("+----------------------------------+");
	
	
	// --- print stack
	while (!s.isEmpty()) {
		std::cout << s.pop() << " ";
	}
	cout << endl;
	// --- print queue
	while (!q.isEmpty()) {
		std::cout << q.dequeue() << " ";
	}
	cout << endl;
	// --- print deque
	while (!d.isEmpty()) {
		std::cout << d.popBack() << " ";
	}
	cout << endl;
	return 1;
}
