#ifndef __STACK__
#define __STACK__

#include "sequence.hpp"

template <typename TYPE>
class Stack {
	Sequence<TYPE> seq;
	
public:
	Stack()  {};
	
	~Stack() {};

	TYPE top() {return seq.getFirst();}
	
	bool push(TYPE value) {return seq.addLast(value);}
	
	TYPE pop() {return seq.removeLast();}

	int getSize() {return seq.getSize();}
	
	bool isEmpty() {return seq.isEmpty();}
	
	void print() {seq.print();}
	
	string toString(){
		string s = "{ ";
		for (int i = 0; i < getSize(); i++){
			s+= seq.get(i);
			if(i+1!=getSize()){
				s+= ", ";
			}
		}
		s += " }";
		return s;
	}
	
	//template<class U> 
	//friend ostream& operator << (ostream &out, Stack<U>  *q){ 
	//	out << q->toString();
		//return out; 
   // };
	
//	template<class U> 
//	friend ostream& operator << (ostream &out, Stack<U> const &q){ 
	//	out << q.toString();
	//	return out; 
   // };
};

#endif
