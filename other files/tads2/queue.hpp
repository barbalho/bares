#ifndef __QUEUE__
#define __QUEUE__

#include "sequence.hpp"

template <typename TYPE>
class Queue {
	Sequence<TYPE> seq;

public:
	Queue()  {}
	
	~Queue() {}

	TYPE front() {return seq.getFirst();}
	
	bool enqueue(TYPE value) {return seq.addLast(value);}
	bool enqueue(Queue<TYPE> *q) {
		for(int i = 0; i < q->getSize(); i++){
			seq.addLast(q->dequeue());
		}
		return true;
	}
	
	TYPE dequeue() {return seq.removeFirst();}

	int getSize() {return seq.getSize();}
	
	bool isEmpty() {return seq.isEmpty();}
	
	void print() {seq.print();}
	
	string toString(){
		string s = "{ ";
		for (int i = 0; i < getSize(); i++){
			s+= seq.get(i);
			if(i+1!=getSize()){
				s+= ", ";
			}
		}
		s += " }";
		return s;
	}
	
	template<class U> 
	friend ostream& operator << (ostream &out, Queue<U>  *q){ 
		out << q->toString();
		return out; 
    };
	
	template<class U> 
	friend ostream& operator << (ostream &out, Queue<U> const &q){ 
		out << q.toString();
		return out; 
    };
};

#endif
