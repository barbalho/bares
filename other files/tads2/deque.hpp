#ifndef __DEQUE__
#define __DEQUE__

#include "sequence.hpp"

template <typename TYPE>
class Deque {
	Sequence<TYPE> seq;
public:
	Deque() {}
	~Deque() {}

	TYPE front() {return seq.getFirst();}
	
	TYPE back() {return seq.getLast();}

	bool pushFront(TYPE value) {return seq.addFirst(value);}

	bool pushBack(TYPE value) {return seq.addLast(value);}

	TYPE popFront() {return seq.removeFirst();}
	
	TYPE popBack() {return seq.removeLast();}

	int getSize() {return seq.getSize();}
	
	bool isEmpty() {return seq.isEmpty();}
	
	void print() {seq.print();}
	
	string toString(){
		string s = "{ ";
		for (int i = 0; i < getSize(); i++){
			s+= seq.get(i);
			if(i+1!=getSize()){
				s+= ", ";
			}
		}
		s += " }";
		return s;
	}
	
	template<class U> 
	friend ostream& operator << (ostream &out, Deque<U>  *q){ 
		out << q->toString();
		return out; 
    };
	
	template<class U> 
	friend ostream& operator << (ostream &out, Deque<U> const &q){ 
		out << q.toString();
		return out; 
    };
};

#endif
