#include "sequence.hpp"
#include <iostream>
using namespace std;
template <typename TYPE>
Sequence<TYPE>::Sequence() {
	size = 0;
}
template <typename TYPE>
Sequence<TYPE>::Sequence(const Sequence<TYPE> &s){
	for(int i = 0; i < s.getSize(); i++){
		//cout << s.get(i) << endl;
		addLast(s.get(i));
	}
	size = s.getSize();
}

template <typename TYPE>
bool Sequence<TYPE>::operator==(const Sequence<TYPE> &s){
		for(int i = 0; i < s.getSize(); i++){
			if(s.get(0) != this->get(0))
			return false;
		}
		return true;
}



template <typename TYPE>
Sequence<TYPE>::~Sequence() {
	if(isEmpty()){
		return;
	}else if(getSize()==1){
		delete(list.next);
	}else{
		Node *aux = list.next;
		while(aux->next != nullptr){
			aux = aux->next;
			delete(aux->prev);
		}
		delete(aux);
	}
	
}

template <typename TYPE>
bool Sequence<TYPE>::isEmpty() const{
	
	if(list.next == nullptr)
		return true;
	else 
		return false;
	
}

template <typename TYPE>
int Sequence<TYPE>::getSize() const{

	/*Node *aux = list.next;
	int cont = 0;
	while(aux != nullptr){
		cont++;
		aux = aux->next;
	}*/
	return size;
}

template <typename TYPE>
bool Sequence<TYPE>::addFirst(const TYPE &value) {

	Node *aux = new Node;
	aux->data = value;
	aux->next = list.next;
	if(isEmpty())
		list.prev = aux;
	else
		list.next->prev = aux;
	list.next = aux;
	size++;
	return true;
}

template <typename TYPE>
bool Sequence<TYPE>::addLast(const TYPE &value) {

	Node *aux = new Node;
	aux->data = value;
	aux->prev = list.prev;
	if(isEmpty())
		list.next = aux;
	else
		list.prev->next = aux;
	list.prev = aux;
	size++;
	return true;
}

template <typename TYPE>
bool Sequence<TYPE>::add(const TYPE &value, int pos) {

	if(pos==0){
		addFirst(value);
	}else if(pos==getSize()-1){
		addLast(value);
	}else{
		Node *temp = list.next;
	
		while(pos > 0){
			temp = temp->next;
			pos--;
		}
		Node *aux = new Node;
		aux->data = value;
		aux->prev = temp->prev;
		aux->next = temp;
		temp->prev->next = aux;
		temp->prev = aux;
	}
	size++;
	return true;
}

template <typename TYPE>
TYPE Sequence<TYPE>::removeFirst() {

	Node *aux = list.next;
	TYPE data = list.next->data;
	if(aux != nullptr){
		list.next = aux->next;
		if(aux->next != nullptr)
			aux->next->prev = nullptr;
		if(list.prev == aux)
			list.prev = nullptr;
		
	delete aux;
	size--;
	return data;
	}
}

template <typename TYPE>
TYPE Sequence<TYPE>::removeLast() {
	Node *aux = list.prev;
	TYPE data = list.prev->data;
	if(aux != nullptr){
		list.prev = aux->prev;
		if(aux->prev != nullptr)
			aux->prev->next = nullptr;
		if(list.next == aux)
			list.next = nullptr;
		
	delete aux;
	size--;
	return data;
	}

}

template <typename TYPE>
TYPE Sequence<TYPE>::remove(int pos) {
	TYPE value;
	if(getSize() > 1){
		Node *temp = list.next;
		while(pos > 0){
			temp = temp->next;
			pos--;
		}
		if(temp->prev == nullptr){
			list.next = temp->next;
			temp->next->prev = nullptr;
		}else if(temp->next != nullptr){
			temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
		}
		if(temp->next == nullptr){
			temp->prev->next = nullptr;
			list.prev = temp->prev;
		}else if(temp->prev != nullptr){
			temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
		}
		value = temp->data;
		delete(temp);
	}else if(getSize() == 1){
		value = list.next->data;
		delete list.next;
		list.next = list.prev = nullptr;
	}
	size--;
	return value;
}

template <typename TYPE>
TYPE Sequence<TYPE>::getFirst() const{
	return list.next->data;
}

template <typename TYPE>
TYPE Sequence<TYPE>::getLast() const{
	return list.prev->data;
}

template <typename TYPE>
TYPE Sequence<TYPE>::get(int pos) const{
	Node *aux = list.next;
	if(pos >= 0 && pos < getSize()  ){
		int cont = 0;
		while(cont != pos){
			cont++;
			aux = aux->next;
		}
	}
	return aux->data;
}

template <typename TYPE>
int Sequence<TYPE>::search(const TYPE &elm) const{
	if(getSize()==0)
		return -1;
	
	Node *aux = list.next;
	int pos = 0;
	while(aux->data != elm){
		if(pos > getSize())
			return -1;
		aux = aux->next;
		pos++;
	}
	return pos;
}

template <typename TYPE>
bool Sequence<TYPE>::isEqual(Sequence<TYPE> &s) const{
	
	if(getSize() != s.getSize())
		return false;
		
	for(int i = 0; i < getSize(); i++)
		if(get(i) != s.get(i))
			return false;
	
	return true;
}

template <typename TYPE>
void Sequence<TYPE>::reverse() {
	Node *n1 = list.next;
	Node *n2 = list.prev;
	for (int i = 0; n1 != nullptr && n2 != nullptr && i < (getSize()/2); i++) {
		Node *no_1 = n1->next;
		Node *no_2 = n2->prev;
		swap(n1, n2); 
		n1 = no_1;
		n2 = no_2;
	}
}



template <typename TYPE>
void Sequence<TYPE>::sort() {
	
	for (Node *i = list.next; i->next != nullptr; i=i->next){
		for (Node *j = i; j != nullptr; j=j->next){
			if(i->data > j->data){
				//swap(i, j);
				TYPE aux = j->data;
				j->data = i->data;
				i->data = aux;
			}
		}
	}
}

template <typename TYPE>
void Sequence<TYPE>::print() const{
	Node *aux = list.next;
	cout << "[ ";
	while (aux != nullptr){
		cout << aux->data << " ";
		aux = aux->next;
	}
	cout << "]"<<endl;
}

template <typename TYPE>
bool Sequence<TYPE>::isIncreasing() const{
	for(int i = 0; i < getSize()-1;i++){
		if(get(i)>get(i+1))
			return false;
	}
	return true;
}

template <typename TYPE>
bool Sequence<TYPE>::isDecreasing() const{
	for(int i = 0; i < getSize()-1;i++){
		if(get(i)<get(i+1))
			return false;
	}
	return true;
}

template <typename TYPE>
void Sequence<TYPE>::bounds(TYPE &min, TYPE &max) const{
	if(!isEmpty()){
		min = max = get(0);
		for(int i = 1; i < getSize(); i++){
			if(get(i) < min)
				min = get(i);
			if(get(i) > max)
				max = get(i);
		}
	}
}
