´´´¶¶¶¶´´´´´´´´´´s¶¶¶¶¶´´´´´´´´´´´s¶¶¶
´´´´¶¶¶¶¢´´´´´7¶¶¶¶¶¶¶¶¶¶¶¶¶´´´´´´´¶¶¶¶
´´´7¶¶¶¶¢´´´¢¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶´´´´s¶¶¶¶s
´´¶¶¶¶¶¶¶¶´ø¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶1´¶¶¶¶¶¶¶¶
´´¢øs$¶¶¶¶1¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶´¶¶¶¶¢¢$$
´´´´´´´´7¢ø¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶s´ø
´´´´´´´´´´¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
´´´´´´´´´´1¶¶¶¶ø´´7¶¶¶¶¶1´ø¶¶¶¶¶s
´´´´´´´´´´´¶¶´´´´´´´¶¶¶´´´´´´s¶¶
´´´´´´´´´´1¶¶´´´´´´$¶¶¶1´´´´´´¶¶1
´´´´´´´´´´´¶¶¶´´s¶¶¶´´ø¶¶s´´¶¶¶¶
´´´´´´´´´´´7¶¶¶¶¶¶¶¶´´´¶¶¶¶¶¶¶¶1
´´´´´´´´´´´´´¶¶¶¶¶¶¶s$s¶¶¶¶¶¶
´´´´´´´´´´´ø¶´¶s¶¶¶¶¶¶¶¶¶¶¶´¶´¶s
´´´´7´´´´$¶¶¶´¶´´´´´´´´´´´$´¶¶¶¶¶
´1¶¶¶¶¶¶¶¶¶¶ø´¶´¶¶$¶¶$¶¶$¶7¶1´¶¶¶¶¶¶¶¶¶¶¶
´´¶¶¶¶¶¶¶¶´´´´¶¶¶¶¶¶¶¶¶¶¶¶¶¶1´´´¶¶¶¶¶¶¶¶¶
´´´ø¶¶¶¶¶´´´´´´1¶¶¶¶¶¶¶¶¶¶¢´´´´´´¶¶¶¶¶¶¶
´´´´´s¶¶ø´´´´´´´´´$¶¶¶¶¶s´´´´´´´´1¶¶¶




-- Desenvolvedores:

    Felipe Barbalho Rocha
    Raul Silveira Silva

-----------------------------------------------------------------------------------------
-- Informações 
    * O programa Executa operação contida no arquivo na pasta 'data' no arquivo "in.txt";
    * Cada quebra de linha é uma ordem de Execução do formato infixo para posfixo;
    * Resultados das operações (uma por linha) está na pasta 'data' no arquivo "out.txt";

-----------------------------------------------------------------------------------------
-- Executa a aplicação: 
    * comando "./make" na raiz do diretório "bares";

-----------------------------------------------------------------------------------------
-- Enumeração de Tratamento de Erros:

    0. Falta Operador
    1. Constante Numérica Inválida
    2. Falta Operando
    3. Falta Operador
    4. Fechamento de escopo inválido
    5. Falta Operando
    6. Operador Inválido
    7. Operando Inválido
    8. Falta Operando
    9. Escopo Aberto

-----------------------------------------------------------------------------------------
-- Limitações da aplicação
    * Expressões como (1+2)/2 que possuam divisão ou multiplicação após o fechamento de parênteses não funciona.m
    *



